// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import SuiVue from 'semantic-ui-vue'
import VueScrollTo from 'vue-scrollto'
import 'semantic-ui-css/semantic.min.css'
import AOS from 'aos'
import 'aos/dist/aos.css'
import App from './router/App'
import router from './router'

Vue.config.productionTip = false;
Vue.use(SuiVue);
Vue.use(VueScrollTo);

/* eslint-disable no-new */
new Vue({
  created(){
    AOS.init();
  },
    mounted(){
        if (window.matchMedia('(min-width: 768px)').matches){
            AOS.init();
        }
        else if(window.matchMedia('(max-width: 767.99px)').matches){
            AOS.init({disable: true});
        }
    },
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
