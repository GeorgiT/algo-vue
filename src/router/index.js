import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import Team from '@/pages/Team'
import Careers from '@/pages/Careers'
import ContactUs from '@/pages/ContactUs'
import Works from '@/pages/Works'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/team',
      name: 'Team',
      component: Team
    },
    {
      path: '/works',
      name: 'Works',
      component: Works
    },
    {
      path: '/careers',
      name: 'Careers',
      component: Careers
    },
    {
      path: '/contact',
      name: 'Contact',
      component: ContactUs
    },
    {
      path: '/works',
      name: '/Works',
      component: Works
    }
  ]
})
